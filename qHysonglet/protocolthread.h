#ifndef PROTOCOLTHREAD_H
#define PROTOCOLTHREAD_H

#include <QThread>
#include <QByteArray>


class ProtocolThread:public QThread
{
     Q_OBJECT

public slots:
    void RecvDataBytes(const QByteArray &bytes);

public:
    ProtocolThread();

    private:
    void run() override;

};

#endif // PROTOCOLTHREAD_H
