﻿

#ifndef _FRAME_H
#define _FRAME_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdint.h>
#include "frame_cmd_def.h"
#include "frame_porting.h"

#define FRAME_CHECK_SUM_256                     1
#define FRAME_CHECK_CRC_8                       2
#define FRAME_CHECK                             FRAME_CHECK_CRC_8
#define FRAME_LITTLE_ENDIAN                     1

/* 使用数据顺序crc校验解码和编码，crc不一致则解码出错 */
#define FRAME_USING_DATA_TYPE_CRC               1

/* 数据长度类型 */
typedef uint16_t frame_len_t;
typedef uint16_t frame_sof_t;

#if((FRAME_CHECK == FRAME_CHECK_CRC_8) || (FRAME_CHECK == FRAME_CHECK_SUM_256))
typedef uint8_t frame_crc_t;
#else
#error "have not develop others crc"
#endif

/* 一帧数据最长为32个字节 */
#define FRAME_BUFF_MAX_LEN			1024

#define FRAME_DATA_FEILD_MAX_LEN    (FRAME_BUFF_MAX_LEN - FRAME_BUFF_MIN_LEN)
/* 帧数据最小的长度  */
#if (FRAME_USING_DATA_TYPE_CRC == 1)
#define FRAME_HEAD_LEN              (sizeof(frame_sof_t) + sizeof(frame_cmd_t) +  \
    sizeof(frame_len_t) + sizeof(frame_crc_t) +\
    sizeof(frame_crc_t))
#define FRAME_DATA_CRC_GET(f)   	get_uint8(f->data_type_crc)
#else
#define FRAME_HEAD_LEN              (sizeof(frame_sof_t) + sizeof(frame_cmd_t) + \
    sizeof(frame_len_t) + sizeof(frame_crc_t))
#endif
#define FRAME_BASE_HEAD_LEN         (FRAME_HEAD_LEN - sizeof(frame_sof_t))
#define FRAME_BUFF_MIN_LEN          (FRAME_HEAD_LEN + sizeof(frame_sof_t))

#define FRAME_SOF                   (frame_sof_t)(0xA5A5)
#define FRAME_EOF                   (frame_sof_t)(0x5A5A)

#pragma pack(1)
typedef struct _frame{
    uint8_t sof[sizeof(frame_sof_t)];
    uint8_t crc[sizeof(frame_crc_t)];
    /* 命令号: bit[24~31]
    命令序号（参数）:  bit[0:23]*/
	uint8_t cmd_para[sizeof(frame_cmd_t)];   
    /* 数据长度，数据域的长度 */
    uint8_t len[sizeof(frame_len_t)];
    /* 数据校验域 */
    /* 数据类型排列校验位 */
#if (FRAME_USING_DATA_TYPE_CRC == 1)
    uint8_t data_type_crc[sizeof(frame_crc_t)];
#endif
	/* 数据校验, 可选 */
    uint8_t data[FRAME_DATA_FEILD_MAX_LEN];
}frame_t;

/* 错误结果 */
typedef enum{
    RESULT_OK = 0,
    RESULT_ERR,
    RESULR_ERR_SOF,
    RESULT_ERR_EOF,
    RESULR_ERR_CMD,
    RESULR_ERR_LEN,
    RESULR_ERR_CRC,
    RESULR_ERR_DATA_CRC,
}result_t;

#define DATA_ITEM(type)	DATA_ITEM_##type

typedef char *    string_t;
typedef uint8_t * bytes_t;

typedef enum {
    DATA_ITEM(int8_t),
    DATA_ITEM(uint8_t),
    DATA_ITEM(char),
    DATA_ITEM(int16_t),
    DATA_ITEM(uint16_t),
    DATA_ITEM(int32_t),
    DATA_ITEM(uint32_t),
    DATA_ITEM(int64_t),
    DATA_ITEM(uint64_t),
    DATA_ITEM(float),
    DATA_ITEM(double),
    DATA_ITEM(string_t),
    DATA_ITEM(bytes_t),
    DATA_ITEM(bool),
}data_item_t;

typedef bool (*frame_handle_fun_t)(frame_t *frame);

typedef struct _frame_handle{
    frame_cmd_t cmd_para;
    frame_handle_fun_t fun;
}frame_handle_t;


#define FRAME_DATA_SET(DATA_TYPE, DATA)         \
            DATA_ITEM(DATA_TYPE), (DATA_TYPE)DATA

#define FRAME_DATA_READ(DATA_TYPE, VAR)         \
            DATA_ITEM(DATA_TYPE), (void *)&(VAR)


frame_t *raw_buf_to_frame(const uint8_t *raw, size_t len);
result_t frame_is_valid(frame_t *f);
void frame_crc_update(frame_t *f);
void frame_debug_print(frame_t *f);

int frame_read(frame_t *f, frame_cmd_t cmd_para, size_t data_cnt, ...);
int frame_set(frame_t *f, frame_cmd_t cmd_para, size_t data_cnt, ...);
bool frame_trans(const frame_t *f);
bool frame_bytes_is_eof(uint8_t byte[sizeof(frame_sof_t)]);
bool frame_bytes_is_sof(uint8_t byte[2]);
frame_cmd_t frame_get_cmd_para(const frame_t *f);
uint32_t frame_get_len(const frame_t *f);
uint32_t frame_buff_len(const frame_t *f);

#define ARG(t)					t
#define ARG_N(a1, a2 ,a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15,\
    a16, a17, a18, a19, a20, N, ...)	N
#define _ARG_N(...)			ARG(ARG_N(__VA_ARGS__))
#define COUNT_ARG(...)			_ARG_N(__VA_ARGS__, 20, 19, 18, 17, 16, 15, 14, \
    13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
 

#define FRAME_SET_SMART(frame, ...)			\
    frame_set_smart(frame, COUNT_ARG(__VA_ARGS__), ##__VA_ARGS__)
#define FRAME_GET_SMART(frame, ...)			\
    frame_read_smart(frame, COUNT_ARG(__VA_ARGS__), ##__VA_ARGS__)
 
int frame_set_smart(frame_t *f, size_t data_cnt, ...);
int frame_read_smart(frame_t *f, size_t data_cnt, ...);
//bool frame_trans(const frame_t *f);
#define ASSERT  assert

#ifdef __cplusplus
}
#endif

#endif


