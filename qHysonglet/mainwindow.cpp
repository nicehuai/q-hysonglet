﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_jsontoframelite.h"

#include <QtSerialPort/QSerialPortInfo>
#include <QDateTime>
#include <QTextCursor>
#include <QScrollBar>
#include <QClipboard>

#include <QStandardPaths>

#include <QStandardItemModel>

#include <QJsonArray>
#include <QJsonValue>

#include <QFile>
#include <QDir>

#include <QDebug>

#include "frame.h"

#define STATUS_MESSAGE_SHOW_MS       2000

#define DATA_TYPE_STRING_LIST       (QStringList()<<"int8_t"<<"uint8_t"<<"char"<<"int16_t"<<"uint16_t"<<"int32_t"<<"uint32_t"<<"float"<<"double"<<"string"<<"bytes")

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/res/ico.ico"));

    uiInit();

    connect(&m_masterSerial, &MasterThread::signalSerialDataRead,
            this, &MainWindow::slotSerialDataRead);
    connect(&m_masterSerial, &MasterThread::signalSerial,
            this, &MainWindow::slotSerial);

    connect(this, &MainWindow::signalOpenSerial,
            &m_masterSerial, &MasterThread::openSerial);

    connect(this, &MainWindow::signalCloseSerial,
            &m_masterSerial, &MasterThread::closeSerial);
    connect(this, &MainWindow::signalSendSerialData,
            &m_masterSerial, &MasterThread::slotSendSerialData);

    connect(&timerSendSerial, &QTimer::timeout,
            this, &MainWindow::on_pushButtonSendData_clicked);

    connect(&m_masterSerial, &MasterThread::signalSerialDataRead,
            &m_protocolThread, &ProtocolThread::RecvDataBytes);

    loadSettingFile();

    m_protocolThread.start();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadJsonToTreeWidget(QJsonDocument &sendRecordDoc)
{
    ui->treeWidgetQuickDefinition->takeTopLevelItem(0);

    ui->treeWidgetQuickDefinition->setHeaderLabels(QStringList() << "名称" << "格式" << "数值");

    QJsonObject obj = sendRecordDoc.object();

    QJsonArray recordArray;
    /* 添加record */
    QTreeWidgetItem *item;
    item = new QTreeWidgetItem(ui->treeWidgetQuickDefinition);
    item->setText(0, "record");
    ui->treeWidgetQuickDefinition->addTopLevelItem(item);

    recordArray = obj["record"].toArray();
    if(recordArray.size()){
        for(int i = 0; i < recordArray.size(); i++){
            QJsonObject obj;
            obj = recordArray.at(i).toObject();

            QTreeWidgetItem *leaf;

            if((obj["type"].toString() == QString("ascii")) ||
                    ((obj["type"].toString() == QString("hex")))){
                leaf = new QTreeWidgetItem(item);
                leaf->setText(0, obj["name"].toString());
                leaf->setText(1, obj["type"].toString());
                leaf->setText(2, obj["brief"].toString());
                leaf->setToolTip(0, obj["data"].toString());
                item->addChild(leaf);
            }
            else if(obj["type"].toString() == QString("frame")){
                leaf = new QTreeWidgetItem(item);
                leaf->setText(0, obj["name"].toString());
                leaf->setText(1, obj["type"].toString());
                leaf->setText(2, obj["brief"].toString());

                QTreeWidgetItem *frameItem;
                frameItem = new QTreeWidgetItem(leaf);
                frameItem->setText(0, "funcode");
                frameItem->setData(0, Qt::EditRole, "funcode");

                QSpinBox *spinBox;
                spinBox = new QSpinBox;
                spinBox->setDisplayIntegerBase(16);
                spinBox->setPrefix("0x");
                spinBox->setRange(0, INT32_MAX);
                spinBox->setValue(obj["funcode"].toString().toUInt(NULL, 16));
                ui->treeWidgetQuickDefinition->setItemWidget(frameItem, 1, spinBox);

                frameItem->setText(1, obj["funcode"].toString());

                QJsonObject dataObj;
                dataObj = obj["data"].toObject();


                frameItem = new QTreeWidgetItem(leaf);
                frameItem->setText(0, "data");
                leaf->addChild(frameItem);

                QTreeWidgetItem *dataSendItem;
                dataSendItem = new QTreeWidgetItem(frameItem);
                dataSendItem->setText(0, "send");
                dataSendItem->setData(0, Qt::EditRole, "send");
                QJsonArray sendArray;
                sendArray = dataObj["send"].toArray();
                for(int x = 0; x < sendArray.size(); x++){
                    QTreeWidgetItem *_t;
                    _t = new QTreeWidgetItem(dataSendItem);

                    QLineEdit *lineEdit;
                    lineEdit = new QLineEdit;
                    lineEdit->setText(sendArray.at(x)["name"].toString());

                    QComboBox *typeComoBox;
                    typeComoBox = new QComboBox();
                    typeComoBox->addItems(DATA_TYPE_STRING_LIST);
                    typeComoBox->setCurrentText(sendArray.at(x)["type"].toString());

                    QLineEdit *valueLineEdit;
                    valueLineEdit = new QLineEdit;
                    valueLineEdit->setText(sendArray.at(x)["value"].toString());

                    ui->treeWidgetQuickDefinition->setItemWidget(_t, 0, lineEdit);
                    ui->treeWidgetQuickDefinition->setItemWidget(_t, 1, typeComoBox);
                    ui->treeWidgetQuickDefinition->setItemWidget(_t, 2, valueLineEdit);
                }
                frameItem->addChild(dataSendItem);

                QTreeWidgetItem *dataRecvItem;
                dataRecvItem = new QTreeWidgetItem(frameItem);
                dataRecvItem->setText(0, "recv");
                dataRecvItem->setData(0, Qt::EditRole, "recv");
                frameItem->addChild(dataRecvItem);
                QJsonArray recvArray;
                recvArray = dataObj["recv"].toArray();
                for(int x = 0; x < recvArray.size(); x++){
                    QTreeWidgetItem *_t;
                    _t = new QTreeWidgetItem(dataRecvItem);

                    QLineEdit *lineEdit;
                    lineEdit = new QLineEdit;
                    lineEdit->setText(recvArray.at(x)["name"].toString());

                    QComboBox *typeComoBox;
                    typeComoBox = new QComboBox();
                    typeComoBox->addItems(DATA_TYPE_STRING_LIST);
                    typeComoBox->setCurrentText(recvArray.at(x)["type"].toString());

                    ui->treeWidgetQuickDefinition->setItemWidget(_t, 0, lineEdit);
                    ui->treeWidgetQuickDefinition->setItemWidget(_t, 1, typeComoBox);
                }
                frameItem->addChild(dataRecvItem);

                item->addChild(leaf);
            }
            else{

            }
        }
    }
}

void MainWindow::loadSettingFile(QString filePath)
{
    QString fileData;

    if(filePath.length() == 0){
        filePath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation)
                         + QDir::separator() + SEND_RECORD_FILE_NAME;
    }

    QFile sendRecordFile(filePath);
    sendRecordFile.open(QIODevice::ReadOnly);
    if(sendRecordFile.isOpen() == false){
        return;
    }
    statusBar()->showMessage(tr("读取配置：") + filePath, STATUS_MESSAGE_SHOW_MS);
    fileData = sendRecordFile.readAll();
    sendRecordFile.close();

    /* 转换json结构 */
    sendRecordDoc = QJsonDocument::fromJson(fileData.toUtf8());

    loadJsonToTreeWidget(sendRecordDoc);
}

void MainWindow::saveSettingFile(QString filePath)
{
    filePath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation)
                         + QDir::separator() + SEND_RECORD_FILE_NAME;

    QFile sendRecordFile(filePath);

    sendRecordFile.open(QIODevice::WriteOnly);
//    sendRecordFile.write(sendRecordDoc.toJson(QJsonDocument::Compact));
    sendRecordFile.write(sendRecordDoc.toJson());
    sendRecordFile.flush();
    sendRecordFile.close();
}

void MainWindow::uiInit(void)
{
    rxCount = 0;
    txCount = 0;

    setWindowTitle(tr("qHysonglet"));

    ui->comboBoxSerial->clear();
    ui->pushButtonSerialOpenOrClose->setText(tr("打开"));
    ui->pushButtonSerialOpenOrClose->setStyleSheet("background: red");
    ui->textEditRecv->setReadOnly(true);
    ui->pushButtonQuickDefinitionAdd->setEnabled(false);

    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts()){
        ui->comboBoxSerial->addItem(info.portName());
    }
    ui->comboBoxSerialBaud->clear();
    foreach(qint32 baud, QSerialPortInfo::standardBaudRates()){
        ui->comboBoxSerialBaud->addItem(QString::asprintf("%d", baud));
    }

    ui->comboBoxSerialBaud->setCurrentText("115200");
    ui->checkBoxRollScreen->setChecked(true);

    ui->pushButtonSendData->setDisabled(true);
    ui->checkBoxAutoSend->setCheckable(false);
}

void MainWindow::textRecvWriteMessage(const QString &msg, bool wrap, bool isHex,
                                      bool showTime, bool roll, QColor color)
{
    int old_val;
    old_val = ui->textEditRecv->verticalScrollBar()->value();
    ui->textEditRecv->moveCursor(QTextCursor::End);
    ui->textEditRecv->setTextColor(color);
    if(showTime){
        ui->textEditRecv->append(QDateTime::currentDateTime().toString("[mm/dd hh:mm:ss.zzz]\r\n"));
    }

    if(isHex){
        ui->textEditRecv->insertPlainText(msg.toUtf8().toHex(' '));
//        ui->textEditRecv->insertPlainText(msg.toLocal8Bit().toHex(' '));
    }
    else{
        ui->textEditRecv->insertPlainText(msg);
    }
    if(wrap){
        ui->textEditRecv->insertPlainText("\r\n");
    }
    if(roll){
        ui->textEditRecv->moveCursor(QTextCursor::End);
    }
    else{
        ui->textEditRecv->verticalScrollBar()->setValue(old_val);
    }
}

void MainWindow::textRecvWriteMessage(const QByteArray &msg, bool wrap, bool isHex,
                                      bool showTime, bool roll, QColor color)
{
    int old_val;
    old_val = ui->textEditRecv->verticalScrollBar()->value();
    ui->textEditRecv->moveCursor(QTextCursor::End);
    ui->textEditRecv->setTextColor(color);
    if(showTime){
        ui->textEditRecv->append(QDateTime::currentDateTime().toString("[mm/dd hh:mm:ss.zzz]\r\n"));
    }

    if(isHex){
        ui->textEditRecv->insertPlainText(msg.toHex(' '));
    }
    else{
        ui->textEditRecv->insertPlainText(msg);
    }
    if(wrap){
        ui->textEditRecv->insertPlainText("\r\n");
    }
    if(roll){
        ui->textEditRecv->moveCursor(QTextCursor::End);
    }
    else{
        ui->textEditRecv->verticalScrollBar()->setValue(old_val);
    }
}

void MainWindow::on_pushButtonClearRecv_clicked()
{
    ui->textEditRecv->clear();
}

void MainWindow::on_pushButtonClearSend_clicked()
{
    ui->textEditSend->clear();
}

void MainWindow::on_comboBoxSerial_activated(const QString &arg1)
{
    QSerialPortInfo info;
    info = QSerialPortInfo(arg1);
    this->statusBar()->showMessage(info.description() + "\t(" +
                                     info.manufacturer() + ")",
                                   STATUS_MESSAGE_SHOW_MS);
}

void MainWindow::slotSerial(QSerialPort::SerialPortError err, QString errString)
{
    QString statusString;
    if(m_masterSerial.serailIsOpen() == false){
        ui->pushButtonSerialOpenOrClose->setText(tr("打开"));
        ui->pushButtonSerialOpenOrClose->setStyleSheet("background: red");

        ui->comboBoxSerial->setEnabled(true);
        ui->comboBoxSerialBaud->setEnabled(true);
        ui->pushButtonSendData->setEnabled(false);
        ui->checkBoxAutoSend->setChecked(false);
        ui->checkBoxAutoSend->setCheckable(false);

        statusString = ui->comboBoxSerial->currentText() + tr(" 已关闭");
    }
    else{
        ui->pushButtonSerialOpenOrClose->setText(tr("关闭"));
        ui->pushButtonSerialOpenOrClose->setStyleSheet("background: green");

        ui->comboBoxSerial->setEnabled(false);
        ui->comboBoxSerialBaud->setEnabled(false);
        ui->pushButtonSendData->setEnabled(true);
        ui->checkBoxAutoSend->setCheckable(true);

        statusString = ui->comboBoxSerial->currentText() + tr(" 已打开");
    }
    if(err != QSerialPort::QSerialPort::NoError){
        statusString = ui->comboBoxSerial->currentText() + tr(" 打开失败,原因: ") + errString;
    }
    this->statusBar()->showMessage(statusString, STATUS_MESSAGE_SHOW_MS);
    ui->groupBoxSerial->setStatusTip(statusString);
}

void MainWindow::slotSerialDataRead(const QByteArray &bytes)
{
    rxCount += bytes.length();
    ui->pushButtonResetRecvCount->setText(QString::asprintf("%d/%u", bytes.length(), rxCount));

    if(ui->checkBoxRxPauseShow->isChecked() == true){
        return;
    }
    textRecvWriteMessage(bytes, ui->checkBoxRecvWrap->isChecked(),
                         ui->radioButtonRecvHexShow->isChecked(),
                         ui->checkBoxRecvShowTime->isChecked(),
                         ui->checkBoxRollScreen->isChecked());


}

void MainWindow::slotQuickDefineItemDel(void)
{
    QTreeWidgetItem *curItem;
    curItem = ui->treeWidgetQuickDefinition->currentItem();
    if(curItem == nullptr){
        return;
    }

    QTreeWidgetItem *parent;
    parent = curItem->parent();

    if(parent == nullptr){
        return;
    }

    parent->removeChild(curItem);

    delete  curItem;

}
void MainWindow::slotQuickDefineRename(void)
{

}

void MainWindow::on_pushButtonSerialOpenOrClose_clicked()
{
    if(m_masterSerial.serailIsOpen() == true){
        emit signalCloseSerial();
    }
    else{
        emit signalOpenSerial(ui->comboBoxSerial->currentText(),
                              115200);
    }
}

void MainWindow::on_pushButtonResetRecvCount_clicked()
{
    rxCount = 0;
    ui->pushButtonResetRecvCount->setText(QString::asprintf("%d/%u", 0, rxCount));
}

void MainWindow::on_pushButtonSendData_clicked()
{
    int pos = 0;
    const int block = 2048;
    QByteArray tmp;

    if(ui->checkBoxSendRecord->isChecked()){
        textRecvWriteMessage(ui->textEditSend->toPlainText().toUtf8(),
                         ui->checkBoxSendWrap->isChecked(),
                             false,
                         ui->checkBoxRecvShowTime->isChecked(),
                         ui->checkBoxRollScreen->isChecked(),
                         QColor(0x33CC33));
    }

    if(ui->radioButtonSendHexShow->isChecked()){
        tmp = QByteArray::fromHex(ui->textEditSend->toPlainText().toUtf8());
    }
    else {
        tmp = ui->textEditSend->toPlainText().toLocal8Bit();
    }

    if(ui->checkBoxSendWrap->isChecked()){
        tmp.append("\r\n");
    }


    /* 防止一下爆满发送缓存 */
    do{
        emit signalSendSerialData(tmp.mid(pos, block));
        pos += block;
    }while(tmp.length() > pos);

    txCount += tmp.size();
    ui->pushButtonResetSendCount->setText(QString::asprintf("%d/%u", tmp.size(), txCount));
}

void MainWindow::on_radioButtonSendAsciiShow_toggled(bool checked)
{
    if(checked == true){
        QString doc;
        doc = QByteArray::fromHex(ui->textEditSend->toPlainText().toUtf8());
        ui->textEditSend->clear();
        ui->textEditSend->setText(doc);
    }
}

void MainWindow::on_radioButtonSendHexShow_toggled(bool checked)
{
    if(checked == true){
        QString doc;
        doc = ui->textEditSend->toPlainText().toUtf8().toHex(' ');
        ui->textEditSend->clear();
        ui->textEditSend->setText(doc);
    }
}

void MainWindow::on_checkBoxAutoSend_toggled(bool checked)
{
    if(checked){
        ui->spinBoxAutoSendTimeIntervalMs->setReadOnly(true);
        timerSendSerial.setInterval(ui->spinBoxAutoSendTimeIntervalMs->value());
        timerSendSerial.start();
    }
    else{
        timerSendSerial.stop();
        ui->spinBoxAutoSendTimeIntervalMs->setReadOnly(false);
    }
}

void MainWindow::on_pushButtonResetSendCount_clicked()
{
    txCount = 0;
    ui->pushButtonResetSendCount->setText(QString::asprintf("%d/%u", 0, txCount));
}

void MainWindow::on_lineEditQuickDefinitionName_textChanged(const QString &arg1)
{
    if(arg1.length()){
        ui->pushButtonQuickDefinitionAdd->setEnabled(true);
    }
    else{
        ui->pushButtonQuickDefinitionAdd->setEnabled(false);
    }
}

void MainWindow::on_pushButtonQuickDefinitionAdd_clicked()
{
    QString tmp;
    tmp = ui->textEditSend->toPlainText().toUtf8();
    /* 添加到json */
    if(tmp.length()){

        /* 校验字段 */
        if(ui->radioButtonSendFrameLite->isChecked()){
            QJsonDocument doc;
            QJsonParseError err;
            doc = QJsonDocument::fromJson(ui->textEditSend->toPlainText().toUtf8(), &err);
//            QString tm = ui->textEditSend->toPlainText().toUtf8();
//            doc = QJsonDocument::fromBinaryData(tm.toUtf8());
            do{
                if(err.error != QJsonParseError::NoError){
                    break;
                }
                QJsonObject obj;
                obj = doc.object();

                if((obj.contains("name") == false) ||
                        (obj.contains("funcode") == false) ||
                        (obj.contains("data") == false)){
                    break;
                }

                this->statusBar()->showMessage("OK", STATUS_MESSAGE_SHOW_MS);
                return ;

            }while(0);
            this->statusBar()->showMessage("数据格式错误3", STATUS_MESSAGE_SHOW_MS);
        }
        else if(ui->radioButtonSendHexShow->isChecked()){
            QTreeWidgetItem *item = ui->treeWidgetQuickDefinition->itemAt(0,0);
            QTreeWidgetItem *newItem;
            newItem = new QTreeWidgetItem(item, QStringList()<<ui->lineEditQuickDefinitionName->text());
            newItem->setData(0, Qt::ToolTipRole, ui->textEditSend->toPlainText().toUtf8());
            newItem->setText(1, "hex");
            newItem->setText(2, ui->textEditSend->toPlainText().replace(QRegExp("\n"), "\\n"));
            item->addChild(newItem);
        }
        else if(ui->radioButtonSendAsciiShow->isChecked()){
            QTreeWidgetItem *item = ui->treeWidgetQuickDefinition->itemAt(0,0);
            QTreeWidgetItem *newItem;
            newItem = new QTreeWidgetItem(item, QStringList()<<ui->lineEditQuickDefinitionName->text());
            newItem->setData(0, Qt::ToolTipRole, ui->textEditSend->toPlainText().toUtf8());
            newItem->setText(1, "ascii");
            newItem->setText(2, ui->textEditSend->toPlainText().replace(QRegExp("\n"), "\\n"));
            item->addChild(newItem);
        }
    }
}

void MainWindow::on_listWidgetQuickDefinition_currentTextChanged(const QString &currentText)
{
    QJsonObject obj;

    ui->lineEditQuickDefinitionName->setText(currentText);

    obj = sendRecordDoc.object();
    QJsonArray record;
    record = obj.value("record").toArray();
    for(int i = 0; i < record.size(); i++){
        QJsonObject var;
        var = record.at(i).toObject();
        if(currentText == var.take("name").toString()){
            ui->textEditSend->setText(var.take("data").toString().toUtf8());
            return;
        }
    }
}

void MainWindow::on_pushButtonCopy_clicked()
{
    QApplication::clipboard()->setText(ui->textEditSend->toPlainText().toUtf8());
    this->statusBar()->showMessage(tr("已拷贝到粘贴板"), STATUS_MESSAGE_SHOW_MS);
}

void MainWindow::closeEvent(QCloseEvent *event)
{

    saveSettingFile();
}

void MainWindow::on_listWidgetQuickDefinition_itemDoubleClicked(QListWidgetItem *item)
{
    on_pushButtonSendData_clicked();
}

void MainWindow::on_treeWidgetQuickDefinition_itemClicked(QTreeWidgetItem *item, int column)
{
    QString type;
    type = item->text(1);
    if(type == "hex"){
        ui->radioButtonSendHexShow->setChecked(true);
    }
    else if(type == "ascii"){
        ui->radioButtonSendAsciiShow->setChecked(true);
    }
    else if(type == "frame"){
        ui->radioButtonSendFrameLite->setChecked(true);
    }
    else{
        return ;
    }
    ui->textEditSend->setPlainText(item->data(0, Qt::ToolTipRole).toString());
}

void MainWindow::on_treeWidgetQuickDefinition_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    ui->textEditSend->setPlainText(item->data(0, Qt::ToolTipRole).toString());
    on_pushButtonSendData_clicked();
}

#include <QList>
void MainWindow::on_treeWidgetQuickDefinition_customContextMenuRequested(const QPoint &pos)
{
    QTreeWidgetItem *curItem = ui->treeWidgetQuickDefinition->currentItem();
    if(curItem == NULL){
        return;
    }

    QList<QAction *>actionList;
    qDebug()<<curItem->text(0);
    //dataRecvItem->setData(0, Qt::EditRole, "recv");
    if(curItem->data(0, Qt::EditRole).toString() == QString("recv")){
        actionList.append(new QAction(QString("删除")));
        actionList.append(new QAction(QString("添加")));
    }
    else{
        actionList.append(new QAction(QString("删除")));
    }




    QMenu menu(ui->treeWidgetQuickDefinition);
    menu.addActions(actionList);
    menu.exec(QCursor::pos());
}
