#include "protocolthread.h"

#include "protocol.h"
#include <QDebug>

ProtocolThread::ProtocolThread()
{
    protocol_init();
}

void ProtocolThread::run()
{
    setbuf(stdout, NULL);
    for(;;){
        recv_frame_handle_task((void *)NULL);
        msleep(10);
    }
}

 void ProtocolThread::RecvDataBytes(const QByteArray &bytes)
 {
     for(int i = 0; i < bytes.length(); i++){
         while(recv_rb_add_byte((uint8_t)bytes.at(i)) == false){
             msleep(2);
         }
     }
 }
