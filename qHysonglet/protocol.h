
#ifndef _PROTOCOL_H
#define _PROTOCOL_H

#ifdef __cplusplus
extern "C" {
#endif

/* 构造cmd命令序号 */
#define FRAM_CMD_DEF



/* 命令帧的最大最小名宏定义 */
#define FRAME_CMD_MIN         FRAME_CMD_BEGIN
#define FRAME_CMD_MAX         FRAME_CMD_END

#undef FRAM_CMD_DEF

#include "frame.h"
#include <stdbool.h>


typedef struct _frame frame_t;
bool trans_rb_add_frame(const frame_t *f);
bool recv_rb_add_frame(frame_t *f);
bool protocol_init(void);
void recv_frame_handle_task(void *para);
void trans_frame_handle_task(void *para);
bool trans_rb_get_byte(uint8_t *byte);
bool recv_rb_add_byte(uint8_t byte);

#define FRAME_FUN_DEF(fun)          frame_fun_##fun

bool FRAME_FUN_DEF(BEGIN)(frame_t *frame);
bool FRAME_FUN_DEF(SET_WIFI_SSID_PASSWD)(frame_t *frame);
bool FRAME_FUN_DEF(RESET)(frame_t *frame);
bool FRAME_FUN_DEF(PING_CR_SERVER)(frame_t *frame);

#ifdef __cplusplus
}
#endif

#endif

