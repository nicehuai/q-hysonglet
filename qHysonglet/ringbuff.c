#include "ringbuff.h"
#include <assert.h>

#define ASSERT assert

void ringbuff_init(ringbuff_t *ringbuff, void *addr, uint32_t size, int item_size)
{
	ASSERT(ringbuff);

	ringbuff->addr = addr;
	ringbuff->buff_size = size;
	ringbuff->remin = size;
	ringbuff->read_idx = 0;
	ringbuff->write_idx = 0;

	ringbuff->item_size = item_size;
}

inline void ringbuff_clear(ringbuff_t *ringbuff)
{
	ringbuff->read_idx = ringbuff->write_idx = 0;
	ringbuff->remin = ringbuff->buff_size;
}

inline bool ringbuff_is_full(const ringbuff_t *ringbuff)
{
	return (ringbuff->remin < ringbuff->item_size) ? true : false;
}

inline size_t ringbuff_push(ringbuff_t *ringbuff, void *push)
{
	uint8_t *ptr;
	ptr = (uint8_t *)push;
	if(ringbuff_is_full(ringbuff)) {
		return 0;
	}
	for(uint32_t i = 0; i < ringbuff->item_size; i++) {
		*((uint8_t *)(ringbuff->addr) + ringbuff->write_idx) = *ptr++;
		ringbuff->write_idx = (ringbuff->write_idx + 1)%ringbuff->buff_size;
	}
	ringbuff->remin -= ringbuff->item_size;
	return 1;
}

inline size_t ringbuff_pop(ringbuff_t *ringbuff, void *save)
{
	uint8_t *ptr;
	ptr = (uint8_t *)save;
	if(ringbuff_is_empty(ringbuff) == true) {
		return 0;
	}
	for(int i = 0; i < ringbuff->item_size; i++) {
		*(ptr++) = *((uint8_t *)(ringbuff->addr) + ringbuff->read_idx);
		ringbuff->read_idx = (ringbuff->read_idx + 1)%ringbuff->buff_size;
	}
	ringbuff->remin += ringbuff->item_size;
	return 1;
}

inline size_t ringbuff_push_list(ringbuff_t *ringbuff, void *push, size_t count)
{
	uint8_t *ptr;
	size_t cnt = 0;
	size_t tmp;
	ptr = push;
	for(int i = 0; i < count; i++){
		tmp = ringbuff_push(ringbuff, (void *)ptr);
		if(tmp == 0){
			return cnt;
		}
		ptr += ringbuff->item_size;
		cnt += tmp;
	}
	return cnt;
}

inline size_t ringbuff_pop_list(ringbuff_t *ringbuff, void *save, size_t count)
{
	uint8_t *ptr;
	size_t cnt = 0;
	size_t tmp;
	ptr = save;
	for(int i = 0; i < count; i++){
		tmp = ringbuff_pop(ringbuff, (void *)ptr);
		if(tmp == 0){
			return cnt;
		}
		ptr += ringbuff->item_size;
		cnt += tmp;
	}
	return cnt;
}

bool ringbuff_show(ringbuff_t *ringbuff, void *save, uint32_t offset)
{
	uint8_t *ptr;
	ptr = (uint8_t *)save;
	if(ringbuff_is_empty(ringbuff) == true) {
		return false;
	}
	uint32_t idx = (ringbuff->read_idx + offset)%ringbuff->buff_size;

	for(uint32_t i = 0; i < ringbuff->item_size; i++) {
		*(ptr++) = *((uint8_t *)ringbuff->addr + idx);
		idx = (idx + 1)%ringbuff->buff_size;
	}

	return true;
}

inline bool ringbuff_is_empty(const ringbuff_t *ringbuff)
{
	return (ringbuff->remin == ringbuff->buff_size)? true: false;
}

inline int ringbuff_used_count(const ringbuff_t *ringbuff)
{
	return (ringbuff->buff_size - ringbuff->remin)/ringbuff->item_size;
}

size_t ringbuff_space_count(const ringbuff_t* ringbuff)
{
	return ringbuff->remin/ringbuff->item_size;
}
