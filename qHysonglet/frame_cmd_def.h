/*******************************************************************************
* file name: frame_cmd_def.h
* bref: ?????????????????????????????????????????????
*
*
*
*******************************************************************************/

#ifndef _FRAME_CMD_DEF_H
#define _FRAME_CMD_DEF_H

#ifdef __cplusplus
extern "C" {
#endif

#define FRAME_FUN_DEF(fun)          frame_fun_##fun
#define frame_fun_NULL              NULL

/* ?????????????????????????? */
#define FRAME_CMD_MAKE_LIST									\
                                FRAME_CMD_MAKE(BEGIN,                   0x00, 0x00, NULL)\
                                FRAME_CMD_MAKE(REQ_VERSION,             0x00, 0x01, NULL)\
                                FRAME_CMD_MAKE(RESET,                   0x00, 0x02, NULL)\
                                FRAME_CMD_MAKE(END,         		0xef, 0x00, NULL)


#ifdef  FRAME_CMD_MAKE
#undef  FRAME_CMD_MAKE
#endif
#define FRAME_CMD_MAKE(name, cmd, para, fun)         \
                    FRAME_CMD_##name = (int)(((((uint32_t)cmd) & 0xff) << 24) | (((uint32_t)para) & 0xffffff)),


typedef enum _frame_cmd{
        FRAME_CMD_MAKE_LIST
}frame_cmd_t;
#undef  FRAME_CMD_MAKE


#define FRAME_CMD(name)                 FRAME_CMD_##name

#ifdef __cplusplus
}
#endif

#endif
