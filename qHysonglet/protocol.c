#include <stdio.h>
#include <stdbool.h>
#include "frame.h"
#include "frame_porting.h"
#include "ringbuff.h"
#include <string.h>
#include <stdlib.h>
#include "protocol.h"
#include <stdint.h>

/* 接收帧和发送帧ringbuff的长度(最大帧长度的两倍，则最多可以存放2帧) */
#define FRAME_RECV_RINGBUFF_LEN            (FRAME_BUFF_MAX_LEN*30)
#define FRAME_TRANS_RINGBUFF_LEN           (FRAME_BUFF_MAX_LEN*2)


/* 固定的代码，基本上不需要修改 */

#ifdef FRAME_CMD_MAKE
#undef FRAME_CMD_MAKE
#endif
#define FRAME_CMD_MAKE(name, cmd, para, fun)         \
                    {FRAME_CMD_##name, frame_fun_##fun},

static const frame_handle_t g_frame_handle_tab[] = {
    FRAME_CMD_MAKE_LIST
};
#undef FRAME_CMD_MAKE

/* ringbuff管理收发缓存 */
static ringbuff_t s_frame_rx_rb;
static ringbuff_t s_frame_tx_rb;
/* 收发接收缓冲 */
static uint8_t s_rx_buff[FRAME_RECV_RINGBUFF_LEN];
static uint8_t s_tx_buff[FRAME_TRANS_RINGBUFF_LEN];

bool protocol_init(void)
{
	ringbuff_init(&s_frame_rx_rb, (void *)s_rx_buff, sizeof(s_rx_buff), 
					sizeof(uint8_t));
	ringbuff_init(&s_frame_tx_rb, (void *)s_tx_buff, sizeof(s_tx_buff), 
					sizeof(uint8_t));
	
    return true;
}

/* 将帧送入发送队列，组帧函数调用 */
bool trans_rb_add_frame(const frame_t *f)
{
    ASSERT(f);
	size_t len;
	len = frame_buff_len(f);
	if(ringbuff_space_count(&s_frame_tx_rb) >= len){
		ringbuff_push_list(&s_frame_tx_rb, (void *)f, len);
		return true;
	}
	return false;
}

/* 提供给通信接口的发送接收 */
bool trans_rb_get_byte(uint8_t *byte)
{
	return ringbuff_pop(&s_frame_tx_rb, (void *)byte) == 1? true: false;
}

/* 提供给通信接口的接收字符函数 */
bool recv_rb_add_byte(uint8_t byte)
{
	return ringbuff_push(&s_frame_rx_rb, (void *)&byte) == 1?true: false;
}

/* 中断接收函数调用 */
bool recv_rb_add_bytes(const uint8_t *bytes, const size_t len)
{
    ASSERT(bytes);
	
	if(ringbuff_space_count(&s_frame_rx_rb) >= len){
		ringbuff_push_list(&s_frame_rx_rb, (void *)bytes, len);
		return true;
	}
	
    return false;
}

void trans_frame_handle_task(void *para)
{
	uint8_t ch;
	while(ringbuff_is_empty(&s_frame_tx_rb) == false){
		ringbuff_pop(&s_frame_tx_rb, (void*)&ch);
		/* 调用发送函数将字符发送出去 */
		frame_byte_send(ch);
	}
}

void recv_frame_handle_task(void *para)
{
	uint8_t ptr[sizeof(frame_t)];
	frame_t *frame = NULL;
	bool matched = false;
	while((ringbuff_used_count(&s_frame_rx_rb) >= FRAME_BUFF_MIN_LEN)){
		/* 缓存中存在的数据够取一帧 */
		matched = false;
		for(size_t i = 0; i < sizeof(frame_sof_t); i++){
			ptr[0] = ptr[1] = 0;
		}
        /* 查找到帧头 */
        int timeout = FRAME_WAIT_DATA_COMMING_MS;
#if 1
        /* 定位帧头 */
        while(ringbuff_used_count(&s_frame_rx_rb) > 0){
			ringbuff_pop(&s_frame_rx_rb, (void *)&(ptr[sizeof(frame_sof_t) - 1]));
			if(frame_bytes_is_sof(ptr) == true){
				matched = true;
				break;
			}
			for(size_t i = 0; i < (sizeof(frame_sof_t) - 1); i++){
				ptr[i] = ptr[i + 1];
			}
            while((ringbuff_used_count(&s_frame_rx_rb) == 0) && timeout--){
                frame_delay_ms(1);
            }
		}

        /* 没有匹配到帧头 */
        if(matched == false){
            continue;
        }

        size_t data_len;
        size_t i = 2;
        matched = false;
find:
        while(i < sizeof(frame_t)){
            /* 等待缓冲中有数据 */
            while((ringbuff_is_empty(&s_frame_rx_rb) == true) && (timeout--)){
                frame_delay_ms(1);
            }
            if(ringbuff_is_empty(&s_frame_rx_rb) == false){
                timeout = FRAME_WAIT_DATA_COMMING_MS;
                ringbuff_pop(&s_frame_rx_rb, (void *)(ptr + i));
                i++;
            }
            /* 数据接收超时 */
            else{
                frame_log("read data timeout, readed: %d", i);
                matched = false;
                break;
            }

            /* 还在读取信息头 */
            if(i < FRAME_HEAD_LEN){
                if(i == sizeof(frame_sof_t)){
                    if(frame_bytes_is_sof(ptr) == false){
                        for(int x = 0; x < (i - 1); x++){
                            ptr[x] = ptr[x + 1];
                        }
                        i--;
                    }
                }
            }
            /* 刚好读取完毕信息头 */
            else if(i == FRAME_HEAD_LEN){
                frame = (frame_t *)ptr;
                data_len = frame_get_len(frame);
                /* 长度错误了 */
                if(data_len > FRAME_DATA_FEILD_MAX_LEN){
                    matched = false;
                    frame_log("len err: %d %d", data_len, i);
//                    frame_debug_print((frame_t *)ptr);
                    break;
                }
            }
            /* 读取到帧尾 */
            else if(i == (data_len + FRAME_BUFF_MIN_LEN)){
//                frame_log("tail: %d %x %x", i, ptr[i - 2], ptr[i - 1]);
                /* 帧尾也正确，则解析一帧 */
                if(frame_bytes_is_eof(ptr + i - 2) == true){
                    frame = raw_buf_to_frame(ptr, FRAME_BUFF_MIN_LEN + data_len);
                    /* 帧错误 */
                    if(frame == NULL){
                        matched = false;
                        frame_log("raw to frame err");
                        break;
                    }
                    matched = true;
                    break;
                }
                else{
                    /* 帧尾错误，说明前面有字节丢了 */
                    matched = false;
                    break;
                }
            }
        }

        if(matched == false){
            uint32_t x;
            for(x = 1; x < (i - 1); x++){
                if(frame_bytes_is_sof(ptr + x) == true){
                    frame_log("move i: %d   x: %d", i, x);
                    for(uint32_t y = 0; (y < (i - x)); y++){
                        ptr[y] = ptr[x + y];
                        frame_log("ptr[%d] = %x", y, ptr[y]);
                    }
                    i = (i - x);
                    if(i >= FRAME_HEAD_LEN){
                        data_len = frame_get_len((frame_t *)ptr);
                    }
                    goto find;
                }
            }
            /* 没有匹配到帧头，则保留最后一个字节 */
            ptr[0] = ptr[i - 1];
            i = 1;
            goto find;
        }
#else
#endif
		if(matched == true){
			/* 数据校验错误，帧不匹配 */
			if(frame == NULL){
				continue;
			}
            frame_debug_print(frame);
			bool ret = false;
			frame_cmd_t cmd;
			cmd = frame_get_cmd_para(frame);
			for(size_t i = 0; i < (sizeof(g_frame_handle_tab)/sizeof(frame_handle_t));
				i++){
				/* 依次对比命令号 */
				if(cmd == g_frame_handle_tab[i].cmd_para){
                    frame_log("run: %x", cmd);
                    /* 定位到命令号且解析函数已经实现 */
					if(g_frame_handle_tab[i].fun != NULL){
						ret = g_frame_handle_tab[i].fun(frame);
						break;
					}
					else{
						frame_log("frame cmd: %x have not develop", (uint32_t)cmd);
					}
					break;
				}
			}
		}
	}
}

frame_t *frame_make_result(frame_t *frame, frame_cmd_t cmd, result_t rst, 
    uint8_t *others, size_t others_len)
{
    ASSERT(frame);

    return frame;
}


bool FRAME_FUN_DEF(BEGIN)(frame_t *frame)
{
    return true;
}

bool FRAME_FUN_DEF(RESET)(frame_t *frame)
{
    //frame_t ret_frame;
    /* 填充回复数据 */
    //frame_make_result(&ret_frame, FRAME_CMD(RESET), RESULT_OK, NULL, 0);

    /* 将数据包送入发送缓冲 */
    //trans_rb_add_frame(&ret_frame);
    /* 复位 */
    return true;
}

/* ping 创想云服务器 */
bool FRAME_FUN_DEF(PING_CR_SERVER)(frame_t *frame)
{

    return false;
}
