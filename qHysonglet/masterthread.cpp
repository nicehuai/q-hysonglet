/****************************************************************************
**
** Copyright (C) 2012 Denis Shienkov <denis.shienkov@gmail.com>
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtSerialPort module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "masterthread.h"

#include <QSerialPort>
#include <QTime>
#include <QSerialPortInfo>
#include <QDebug>

MasterThread::MasterThread(QObject *parent) :
    QThread(parent)
{
    m_quit = true;

    sendDataList.clear();
}

MasterThread::~MasterThread()
{
}
void MasterThread::run()
{
    QByteArray tmp;
    QByteArray readBytes;
    int wait_time;
    wait_time = 1000*10*2/m_serial.baudRate();
    if(wait_time == 0){
        wait_time = 2;
    }
    wait_time = 1;
    qDebug()<<wait_time;
    while(m_quit == false) {
        /* 有数据了,读出去 */
        if(sendDataList.isEmpty() == false){
            tmp = sendDataList.takeFirst();
            m_serial.write(tmp);
            m_serial.waitForBytesWritten(1000);
        }
        readBytes.clear();
        while(m_serial.waitForReadyRead(wait_time)){
            do{
                readBytes += m_serial.readAll();
            }while(m_serial.waitForReadyRead(wait_time));
            emit signalSerialDataRead(readBytes);
        }
//        qDebug()<<m_serial.bytesAvailable();
    }
}

void MasterThread::openSerial(const QString &portName, qint32 baud)
{
    if(m_serial.isOpen()){
        m_serial.close();
    }
    m_serial.setPortName(portName);
    m_serial.setBaudRate(baud);
    m_serial.open(QIODevice::ReadWrite);
    if((m_serial.isOpen() == true) && (isRunning() == false)){
        m_quit = false;
        start();
    }
    emit signalSerial(m_serial.error(), m_serial.errorString());
}

void MasterThread::closeSerial()
{
    m_quit = true;
    if(m_serial.isOpen()){
        m_serial.flush();
        m_serial.close();
        m_serial.clearError();
        /* 发送关闭串口完成的信号 */
        emit signalSerial(m_serial.error(), m_serial.errorString());
    }
}

void MasterThread::slotSendSerialData(const QByteArray &bytes)
{
    if(bytes.isEmpty() == false){
        sendDataList.append(bytes);
    }
}

bool MasterThread::serailIsOpen(void)
{
    return m_serial.isOpen();
}
