#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QColor>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QListWidgetItem>
#include <QTreeWidgetItem>
//#include "jsontoframelite.h"
#include "masterthread.h"
#include "protocolthread.h"

#define SEND_RECORD_FILE_NAME                     "sendRecord.json.hysonglet"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonClearRecv_clicked();

    void on_pushButtonClearSend_clicked();
    void on_comboBoxSerial_activated(const QString &arg1);
    void on_pushButtonSerialOpenOrClose_clicked();

signals:
    void signalOpenSerial(const QString portName, qint32 baud);
    void signalCloseSerial(void);
    void signalSendSerialData(const QByteArray &bytes);

private slots:
    void slotSerial(QSerialPort::SerialPortError err, QString errString);
    void slotSerialDataRead(const QByteArray &bytes);
    void slotQuickDefineItemDel(void);
    void slotQuickDefineRename(void);

    void on_pushButtonResetRecvCount_clicked();

    void on_pushButtonSendData_clicked();

    void on_radioButtonSendAsciiShow_toggled(bool checked);

    void on_radioButtonSendHexShow_toggled(bool checked);

    void on_checkBoxAutoSend_toggled(bool checked);

    void on_pushButtonResetSendCount_clicked();

    void on_lineEditQuickDefinitionName_textChanged(const QString &arg1);

    void on_pushButtonQuickDefinitionAdd_clicked();

    void on_listWidgetQuickDefinition_currentTextChanged(const QString &currentText);

    void on_pushButtonCopy_clicked();

    void on_listWidgetQuickDefinition_itemDoubleClicked(QListWidgetItem *item);

    void on_treeWidgetQuickDefinition_itemClicked(QTreeWidgetItem *item, int column);

    void on_treeWidgetQuickDefinition_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_treeWidgetQuickDefinition_customContextMenuRequested(const QPoint &pos);
private:
    void uiInit(void);
    void textRecvWriteMessage(const QString &msg, bool wrap = false, bool isHex = false,
                                bool showTime = false, bool roll = false,
                              QColor color=QColor("Black"));
    void textRecvWriteMessage(const QByteArray &msg, bool wrap = false, bool isHex = false,
                                bool showTime = false, bool roll = false,
                              QColor color=QColor("Black"));
    void loadSettingFile(QString filePath = "");
    void saveSettingFile(QString filePath = "");
    void loadJsonToTreeWidget(QJsonDocument &sendRecordDoc);

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;

    MasterThread m_masterSerial;
    ProtocolThread m_protocolThread;

    /* 定时发送数据 */
    QTimer   timerSendSerial;
    uint32_t rxCount;
    uint32_t txCount;

//    QJsonObject sendRecordJson;
    QJsonDocument sendRecordDoc;
};

#endif // MAINWINDOW_H
