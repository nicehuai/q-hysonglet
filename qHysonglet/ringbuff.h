/*
 * Copyright (c) 2006-2021, Eywen team
 *
 * SPDX-License-Identifier: BSD
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-11     Eywen        the first version
 */

#ifndef RINGBUFF_H
#define RINGBUFF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct _ringbuff {
    void* addr;
    uint32_t buff_size;
    uint32_t write_idx;
    uint32_t read_idx;
    uint32_t remin;
    int item_size;
} ringbuff_t;

void ringbuff_init(ringbuff_t* ringbuff, void* addr, uint32_t size, int item_size);
bool ringbuff_is_full(const ringbuff_t* ringbuff);
size_t ringbuff_push(ringbuff_t* ringbuff, void* push);
size_t ringbuff_pop(ringbuff_t* ringbuff, void* save);
size_t ringbuff_push_list(ringbuff_t *ringbuff, void *push, size_t count);
size_t ringbuff_pop_list(ringbuff_t *ringbuff, void *save, size_t count);
bool ringbuff_is_empty(const ringbuff_t* ringbuff);
void ringbuff_clear(ringbuff_t* ringbuff);
bool ringbuff_show(ringbuff_t* ringbuff, void* save, uint32_t offset);
int ringbuff_used_count(const ringbuff_t* ringbuff);
size_t ringbuff_space_count(const ringbuff_t* ringbuff);

#ifdef __cplusplus
}
#endif

#endif // RINGBUFF_H
