
#ifndef _FRAME_PORTIONG_H
#define _FRAME_PORTIONG_H

//#include "uart.h"
//#include <unistd.h>
//#include <windows.h>

/* user def */
#define frame_delay_ms(ms)                              delayms(ms)
#define FRAME_WAIT_DATA_COMMING_MS			100
#define frame_byte_send(ch)			        //uart1_send_byte(ch)
#define frame_log(fmt, ...)			printf(fmt"\r\n", ##__VA_ARGS__)


static void delayms(int ms)
{
    for(int i = 0; i < ms; i++){
	volatile int n = 100000000;
	while(n--);
    }
}

#endif
