﻿/*******************************************************************************
* 
* file name: frame.c
* breif    : 自定义的协议帧实现模块,包括组包和解包，包校验，包字段提取
* interface: FRAME_SET_SMART/FRAME_GET_SMART
* example  : 设置frame: FRAME_SET_SMART(frame,FRAME_CMD(CMD_NAME), 
								FRAME_DATA_SET(uint16_t, 0xff11),
								FRAME_DATA_SET(bytes_t,  bytes), sizeof(bytes),
								FRAME_DATA_SET(string_t, "123456"));
* 			 读取frame: FRAME_GET_SMART(frame,FRAME_CMD(CMD_NAME), 
*								FRAME_DATA_READ(uint16_t, val_32),
*								FRAME_DATA_READ(bytes_t,  bytes), sizeof(bytes),
*								FRAME_DATA_READ(string_t, string));
* 
* auther   : eywen
* date     : 2022/11/18
* modify   :
* 
*******************************************************************************/


#include "frame.h"
#include <string.h>
#include <stdarg.h>


static frame_crc_t crc_number(const uint8_t *buf, frame_len_t len);


#define FRAME_SOF_GET(f)        		get_uint16((uint8_t *)(f)->sof)
#define FRAME_CRC_GET(f)        		get_uint8((uint8_t *)(f)->crc)
#define FRAME_DATA_GET(f)       		get_uint8(f->data)
#define FRAME_CRC_LEN_GET(f)    		(frame_get_len(f) + FRAME_HEAD_LEN - \
    sizeof(frame_sof_t) - sizeof(frame_crc_t))
#define FRAME_CRC_SET(f, val)               set_uint8(f->crc, val)
#define FRAME_CMD_PARA_SET(f, val)          set_uint32(f->cmd_para, val)
#define FRAME_SOF_SET(f, val)               set_uint16(f->sof, val)
#if (FRAME_USING_DATA_TYPE_CRC == 1)    
#define FRAME_DATA_TYPE_CRC_SET(f, val)     set_uint8(f->data_type_crc, val)
#endif
#define FRAME_LEN_SET(f, val)               set_uint16(f->len, val)
#define FRAME_APPEND_EOF(f)                 set_uint16((&(f->data[0]) + frame_get_len(f)), FRAME_EOF)
#define FRAME_EOF_GET(f)                get_uint16(&(f->data[0]) + frame_get_len(f))
									
									
/**
 * 这个函数用来将裸的二进制数据转换成一条 frame_t 格式的数据帧
 *  
 * 
 * 
 * @param raw  裸的二进制数据
 * @param len  raw二进制数据的长度
 *
 * @return 返回 frame_t *，错误则返回NULL， 正确则返回指针
 */
frame_t *raw_buf_to_frame(const uint8_t *raw, size_t len)
{
    frame_t *ptr = NULL;

    if((len < FRAME_BUFF_MIN_LEN) || (len > FRAME_BUFF_MAX_LEN)){
        return NULL;
    }

    ptr = (frame_t *)raw;
    if(frame_is_valid(ptr) == RESULT_OK){
        return ptr;
    }

    return NULL;
}


/* 从指定地址取一个uint8类型的数据出来 */
static inline uint8_t get_uint8(const uint8_t *ptr)
{
	ASSERT(ptr);
    return *ptr;
}

/* 从指定地址取一个uint16类型的数据出来 */
static inline uint16_t get_uint16(const uint8_t *ptr)
{
	ASSERT(ptr);
#if (FRAME_LITTLE_ENDIAN == 1)
    return ((uint16_t)get_uint8(ptr) << 8) | (get_uint8(ptr + 1));
#else
    return ((uint16_t)get_uint8(ptr + 1) << 8) | (get_uint8(ptr));
#endif
}

/* 从指定地址取一个uint32类型的数据出来 */
static inline uint32_t get_uint32(const uint8_t *ptr)
{
	ASSERT(ptr);
#if (FRAME_LITTLE_ENDIAN == 1)
    return ((uint32_t)get_uint8(ptr + 0)) << 24 | \
		(uint32_t)(get_uint8(ptr + 1)) << 16 | \
		(uint32_t)(get_uint8(ptr + 2) << 8) | \
		(uint32_t)(get_uint8(ptr + 3));
#else
    return ((uint32_t)get_uint8(ptr + 3)) | \
			(uint32_t)(get_uint8(ptr + 2)) << 8 | \
			(uint32_t)(get_uint8(ptr + 1)) << 16 | \
			(uint32_t)(get_uint8(ptr + 0)) << 24;
#endif
}

/* 从指定地址取一个uint64类型的数据出来 */
static inline uint64_t get_uint64(const uint8_t *ptr)
{
	ASSERT(ptr);
#if (FRAME_LITTLE_ENDIAN == 1)
    return ((uint64_t)get_uint8(ptr + 0)) << 56 | \
		(uint64_t)(get_uint8(ptr + 1)) << 48 | \
		(uint64_t)(get_uint8(ptr + 2)) << 40 | \
		(uint64_t)(get_uint8(ptr + 3)) << 32 | \
		(uint64_t)(get_uint8(ptr + 4)) << 24 | \
		(uint64_t)(get_uint8(ptr + 5)) << 16 | \
		(uint64_t)(get_uint8(ptr + 6)) << 8 | \
		(uint64_t)(get_uint8(ptr + 7)) << 0;
#else
    return ((uint64_t)get_uint8(ptr + 7)) << 0 | \
		(uint64_t)(get_uint8(ptr + 6)) << 8 | \
		(uint64_t)(get_uint8(ptr + 5)) << 16 | \
		(uint64_t)(get_uint8(ptr + 4)) << 24 | \
		(uint64_t)(get_uint8(ptr + 3)) << 32 | \
		(uint64_t)(get_uint8(ptr + 2)) << 40 | \
		(uint64_t)(get_uint8(ptr + 1)) << 48 | \
		(uint64_t)(get_uint8(ptr + 0)) << 56;
#endif
}

/* 从指定地址取一个float类型的数据出来 */
static float get_float(const uint8_t *ptr)
{
	ASSERT(ptr);
    uint32_t tmp;
    tmp = get_uint32(ptr);
    return *(float *)(&tmp);
}

/* 从指定地址取一个float类型的数据出来 */
static float get_double(const uint8_t *ptr)
{
	ASSERT(ptr);
    uint64_t tmp;
    tmp = get_uint64(ptr);
    return *(double *)(&tmp);
}

/* 将uint8类型的数写入指定的地址 */
static void inline set_uint8(uint8_t *ptr, uint8_t var)
{
	ASSERT(ptr);
    *ptr = var;
}

/* 将uint16类型的数写入指定的地址 */
static void set_uint16(uint8_t *ptr, uint16_t var)
{
	ASSERT(ptr);
    uint32_t i, offset;

#if (FRAME_LITTLE_ENDIAN == 1)
    for (i = 0, offset = 8 * (sizeof(var) - 1); i < sizeof(var);
         i++, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#else
    for (i = (sizeof(var) - 1), offset = 8 * (sizeof(var) - 1); i >= 0;
         i--, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#endif
}

/* 将uint32类型的数写入指定的地址 */
static void set_uint32(uint8_t *ptr, uint32_t var)
{
	ASSERT(ptr);
    int i, offset;

#if (FRAME_LITTLE_ENDIAN == 1)
    for (i = 0, offset = 8 * (sizeof(var) - 1); i < sizeof(var);
         i++, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#else
    for (i = (sizeof(var) - 1), offset = 8 * (sizeof(var) - 1); i >= 0;
         i--, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#endif
}

/* 将uint64类型的数写入指定的地址 */
static void set_uint64(uint8_t *ptr, uint64_t var)
{
	ASSERT(ptr);
    int i, offset;

#if (FRAME_LITTLE_ENDIAN == 1)
    for (i = 0, offset = 8 * (sizeof(var) - 1); i < sizeof(var);
         i++, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#else
    for (i = (sizeof(var) - 1), offset = 8 * (sizeof(var) - 1); i >= 0;
         i--, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#endif
}

/* 将float类型的数写入指定的地址 */
static void set_float(uint8_t *ptr, float var)
{
	ASSERT(ptr);
    set_uint32(ptr, *(uint32_t *)&var);
}

/* 将double类型的数写入指定的地址 */
static void set_double(uint8_t *ptr, double var)
{
	ASSERT(ptr);
    set_uint64(ptr, *(uint64_t *)&var);
}

result_t frame_is_valid(frame_t *f)
{
    ASSERT(f);
    if(FRAME_SOF_GET(f) != FRAME_SOF){
        return RESULR_ERR_SOF;
    }

    frame_cmd_t cmd;
    cmd = frame_get_cmd_para(f);
    if(((uint32_t)cmd > (uint32_t)FRAME_CMD(END)) ||
            ((uint32_t)cmd < (uint32_t)FRAME_CMD(BEGIN))){
        frame_log("cmd err: %x[%x,%x]", (uint32_t)cmd, (uint32_t)FRAME_CMD(BEGIN),
                  (uint32_t)FRAME_CMD(END));
        return RESULR_ERR_CMD;
    }

    size_t len;
    len = frame_get_len(f);
    if((len > FRAME_DATA_FEILD_MAX_LEN)){
        frame_log("len err: %d", len);
        return RESULR_ERR_LEN;
    }

    frame_crc_t crc;
    crc = crc_number((f->cmd_para), FRAME_CRC_LEN_GET(f));

    if(crc != FRAME_CRC_GET(f)){
        frame_log("crc err: %x:%x", crc, FRAME_CRC_GET(f));
        return RESULR_ERR_CRC;
    }

    if(FRAME_EOF_GET(f) != FRAME_EOF){
        frame_log("eof err: %x", FRAME_EOF_GET(f));
        return RESULT_ERR_EOF;
    }

    return RESULT_OK;
}

void frame_crc_update(frame_t *f)
{
    ASSERT(f);
    ASSERT((frame_get_len(f) <= FRAME_DATA_FEILD_MAX_LEN));

    FRAME_CRC_SET(f, crc_number((f->cmd_para), FRAME_CRC_LEN_GET(f)));
}

/**
 * 这个函数用来将填充帧的各数据域，自动更新crc
 *  
 * 
 * @param cmd_para : 命令号
 * @param data_cnt:  待填充数据的个数
 * @param ...     :  填充的数据类型和数据
 *          exp:   frame_uint8_t, 0x05,
 *                 frame_float_t, 0.125,
 *                 frame_bytes_t, {0x01, 0x02, 0x03}, 3,
 *                 frame_string_t, "strings"   
 *  推荐使用宏防止数据编译优化:  
 *         exp:    frame_set(&frame,                   // 待填充的帧指针 
 *                              FRAME_CMD(CMD_NAME),   // 命令号 
 *                              13,                    // 13个数据
 *                              FRAME_DATA_SET(int8_t,   -1),
 *                              FRAME_DATA_SET(uint8_t,  0xff),
 *                              FRAME_DATA_SET(char_t,   'c'),
 *                              FRAME_DATA_SET(int16_t,   -123),
 *                              FRAME_DATA_SET(uint16_t, 0xff11),
 *                              FRAME_DATA_SET(int32_t,  123),
 *                              FRAME_DATA_SET(uint32_t, 0x123),
 *                              FRAME_DATA_SET(int64_t, -1234),
 *                              FRAME_DATA_SET(uint64_t, 0x12345678),
 *                              FRAME_DATA_SET(float_t,  0.123),
 *                              FRAME_DATA_SET(double_t, 0.123),
 *                              FRAME_DATA_SET(bytes_t,  bytes), sizeof(bytes),
 *                              FRAME_DATA_SET(string_t, "123456"));              
 *
 * @return 返回 : 填充数据的个数
 */
int frame_set(frame_t *f, frame_cmd_t cmd_para, size_t data_cnt, ...)
{
    ASSERT(f);
//    ASSERT(((uint32_t)cmd_para >= (uint32_t)FRAME_CMD(BEGIN)) && 
//		((uint32_t)cmd_para <= (uint32_t)FRAME_CMD(END)));
	
    va_list v1;
    uint8_t *ptr = NULL;
    uint8_t *ptr_max = NULL;
    int ret = 0;
    data_item_t data_type;
    va_start(v1, data_cnt);

#if (FRAME_USING_DATA_TYPE_CRC == 1)
    uint8_t data_type_list[16] = {0};
    size_t data_type_cnt = 0;
#endif

    /* 存放数据的位置 */
    ptr = f->data;
    ptr_max = ptr + FRAME_DATA_FEILD_MAX_LEN;
    /* 填充cmd */
    FRAME_CMD_PARA_SET(f, cmd_para);
    FRAME_SOF_SET(f, FRAME_SOF);

#define _FRAME_DATA_SET(DATA_TYPE, SET_FUN, ARG_TYPE, ARG, PTR)                \
                                do{                                           \
                                    DATA_TYPE data;                           \
                                    data = (DATA_TYPE)va_arg(ARG, ARG_TYPE);  \
                                    SET_FUN(PTR, data);                       \
                                    PTR += sizeof(DATA_TYPE);                 \
                                }while(0)

    /* 循环解析各参数 */
    for(; data_cnt != 0; data_cnt--){
        /* 获取数据类型 */
        data_type = (data_item_t)va_arg(v1, uint32_t);
#if (FRAME_USING_DATA_TYPE_CRC == 1)
        data_type_list[data_type_cnt++] = (uint8_t)data_type;
#endif
        /* 根据不同的数据类型再提取出不同的数据，写入到frame中去 */
        switch (data_type){
            case (DATA_ITEM(int8_t)):
            case (DATA_ITEM(uint8_t)):
            case (DATA_ITEM(char)):
                _FRAME_DATA_SET(uint8_t, set_uint8, uint32_t, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(int16_t)):
            case (DATA_ITEM(uint16_t)):
                _FRAME_DATA_SET(uint16_t, set_uint16, uint32_t, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(int32_t)):
            case (DATA_ITEM(uint32_t)):
                _FRAME_DATA_SET(uint32_t, set_uint32, uint32_t, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(int64_t)):
            case (DATA_ITEM(uint64_t)):
                _FRAME_DATA_SET(uint64_t, set_uint64, uint64_t, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(float)):
                _FRAME_DATA_SET(float, set_float, double, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(double)):
                _FRAME_DATA_SET(double, set_double, double, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(bytes_t)):
                do{
                    size_t cnt;
                    uint8_t *src_ptr;
                    src_ptr = (uint8_t *)va_arg(v1, int);
                    cnt = (size_t)va_arg(v1, int);

                    /* 限制踩内存 */
                    if((ptr - f->data + cnt) > FRAME_DATA_FEILD_MAX_LEN){
                        cnt = FRAME_DATA_FEILD_MAX_LEN - (ptr - f->data);
                    }
                    memcpy(ptr, src_ptr, cnt);
                    ptr += cnt;
                }while(0);
                ret++;
                break;
            case (DATA_ITEM(string_t)):
                do{
                    uint8_t *src_ptr;
                    size_t str_len;
                    src_ptr = (uint8_t *)va_arg(v1, int);
                    str_len = strlen((const char *)src_ptr) + 1;
                    str_len = (str_len <= (ptr_max - ptr))? str_len: (ptr_max - ptr);
                    strncpy((char *)ptr, (char *)src_ptr, str_len);
                    /* 最后的'\0' 记得加*/
                    ptr += str_len;
                }while(0);
                ret++;
                break;
            default:
                // ESP_LOGE(TAG, "frame data type error: %u", (uint32_t)data_type);
                break;
        }
    }
    va_end(v1);

    // set_uint16((uint8_t *)&(f->len), (uint16_t)(ptr - f->data));
    FRAME_LEN_SET(f, (uint16_t)(ptr - f->data));

    // f->len = (frame_len_t)(ptr - f->data);

    if(frame_get_len(f) > FRAME_DATA_FEILD_MAX_LEN){
        // ESP_LOGE(TAG, "data len too long...");
        while(1);
    }

#if (FRAME_USING_DATA_TYPE_CRC == 1)
    /* 计算数据排列crc校验 */
    FRAME_DATA_TYPE_CRC_SET(f, crc_number((uint8_t *)data_type_list, data_type_cnt));
#endif

    /* 更新校验值 */
    frame_crc_update(f);
#undef _FRAME_DATA_SET

    FRAME_APPEND_EOF(f);


    return ret;
}



/**
 * 这个函数用来提取数据域中各数据
 *  
 * 
 * @param data_cnt:  待填充数据的个数
 * #param ...     :  填充的数据类型和数据
 *          exp:   frame_uint8_t, &ptr_u8,
 *                 frame_float_t, &ptr_float,
 *                 frame_bytes_t, &bytes, sizeof(bytes),
 *                 frame_string_t, &string                  
 * 推荐使用宏防止数据编译优化:  
 *         exp:    frame_read(&frame,                  // 待填充的帧指针 
 *                              FRAME_CMD(CMD_NAME),   // 命令号 
 *                              13,                    // 13个数据
 *                              FRAME_DATA_READ(int8_t,   var),
 *                              FRAME_DATA_READ(uint8_t,  var),
 *                              FRAME_DATA_READ(char_t,   var),
 *                              FRAME_DATA_READ(int16_t,  var),
 *                              FRAME_DATA_READ(uint16_t, var),
 *                              FRAME_DATA_READ(int32_t,  var),
 *                              FRAME_DATA_READ(uint32_t, var),
 *                              FRAME_DATA_READ(int64_t,  var),
 *                              FRAME_DATA_READ(uint64_t, var),
 *                              FRAME_DATA_READ(float_t,  var),
 *                              FRAME_DATA_READ(double_t, var),
 *                              FRAME_DATA_READ(bytes_t,  var), sizeof(var),
 *                              FRAME_DATA_READ(string_t, var));
 *
 * @return 返回 : 无
 */
int frame_read(frame_t *f, frame_cmd_t cmd_para, size_t data_cnt, ...)
{
    ASSERT(f);
//	ASSERT(((uint32_t)cmd_para >= (uint32_t)FRAME_CMD(BEGIN)) && 
//	((uint32_t)cmd_para <= (uint32_t)FRAME_CMD(END)));

    va_list v1;
    int ret = 0;
    uint8_t *ptr = NULL;
    void *save_ptr = NULL;
    uint8_t *ptr_max = NULL;
    data_item_t data_type;
    va_start(v1, data_cnt);
#if (FRAME_USING_DATA_TYPE_CRC == 1)
    uint8_t data_type_list[16] = {0};
    size_t data_type_cnt = 0;
#endif

	if(cmd_para != frame_get_cmd_para(f)){
		frame_log("cmd para not match");
		return -1;
	}
	
    /* 存放数据的位置 */
    ptr = f->data;
    ptr_max = f->data + FRAME_DATA_FEILD_MAX_LEN;

#define _FRAME_DATA_READ(SAVE_PTR, DATA_TYPE, GET_FUN, READ_PTR)                 \
                                do{                                             \
                                    *(DATA_TYPE *)SAVE_PTR = GET_FUN(READ_PTR); \
                                    READ_PTR += sizeof(DATA_TYPE);              \
                                }while(0)

    /* 循环解析各参数 */
    for(; data_cnt != 0; data_cnt--){
        /* 获取数据类型 */
        data_type = (data_item_t)va_arg(v1, uint32_t);
#if (FRAME_USING_DATA_TYPE_CRC == 1)
        data_type_list[data_type_cnt++] = (uint8_t)data_type;
#endif
        save_ptr  = (void *)va_arg(v1, uint32_t);
        /* 根据不同的数据类型再提取出不同的数据，写入到frame中去 */
        switch (data_type){
            case (DATA_ITEM(int8_t)):
            case (DATA_ITEM(uint8_t)):
            case (DATA_ITEM(char)):
                _FRAME_DATA_READ(save_ptr, uint8_t, get_uint8, ptr);
                ret++;
                break;
            case (DATA_ITEM(int16_t)):
            case (DATA_ITEM(uint16_t)):
                _FRAME_DATA_READ(save_ptr, uint16_t, get_uint16, ptr);
                ret++;
                break;
            case (DATA_ITEM(int32_t)):
            case (DATA_ITEM(uint32_t)):
                _FRAME_DATA_READ(save_ptr, uint32_t, get_uint32, ptr);
                ret++;
                break;
            case (DATA_ITEM(int64_t)):
            case (DATA_ITEM(uint64_t)):
                _FRAME_DATA_READ(save_ptr, uint64_t, get_uint64, ptr);
                ret++;
                break;
            case (DATA_ITEM(float)):
                _FRAME_DATA_READ(save_ptr, float, get_float, ptr);
                ret++;
                break;
            case (DATA_ITEM(double)):
                _FRAME_DATA_READ(save_ptr, double, get_double, ptr);
                ret++;
                break;
            case (DATA_ITEM(bytes_t)):
                do{
                    size_t cnt;
                    cnt = (size_t)va_arg(v1, uint32_t);
                    /* 限制踩内存 */
                    if((ptr + cnt) > ptr_max){
                        cnt = ptr_max - ptr;
                    }
                    memcpy(save_ptr, ptr, cnt);
                    ptr += cnt;
                }while(0);
                ret++;
                break;
            case (DATA_ITEM(string_t)):
                do{
                    size_t str_len;
                    str_len = strlen((const char *)ptr) + 1;
                    str_len = (str_len < (ptr_max - ptr))? str_len: (ptr_max - ptr);
                    strncpy((char *)save_ptr, (char *)ptr, str_len);
                    ptr += str_len;
                }while(0);
                ret++;
                break;
            default:
                // ESP_LOGE(TAG, "frame data type error: %u", (uint32_t)data_type);
                break;
        }
    }
    va_end(v1);
#undef _FRAME_DATA_READ

#if (FRAME_USING_DATA_TYPE_CRC == 1)
    /* 计算数据排列crc校验 */
    frame_crc_t data_type_crc;
    data_type_crc =  crc_number((uint8_t *)data_type_list, data_type_cnt);
    if(data_type_crc != FRAME_DATA_CRC_GET(f)){
    }
#endif
    return ret;
}

/**
 * 这个函数用来将填充帧的各数据域，自动更新crc
 *  
 * 
 * @param cmd_para : 命令号
 * @param data_cnt:  待填充数据的个数
 * @param ...     :  填充的数据类型和数据
 *          exp:   frame_uint8_t, 0x05,
 *                 frame_float_t, 0.125,
 *                 frame_bytes_t, {0x01, 0x02, 0x03}, 3,
 *                 frame_string_t, "strings"   
 *  推荐使用宏防止数据编译优化:  
 *         exp:    FRAME_SET_SMART(&frame,             // 待填充的帧指针 
 *                              FRAME_CMD(CMD_NAME),    // 命令号 
 *                              FRAME_DATA_SET(int8_t,   -1),
 *                              FRAME_DATA_SET(uint8_t,  0xff),
 *                              FRAME_DATA_SET(char_t,   'c'),
 *                              FRAME_DATA_SET(int16_t,   -123),
 *                              FRAME_DATA_SET(uint16_t, 0xff11),
 *                              FRAME_DATA_SET(int32_t,  123),
 *                              FRAME_DATA_SET(uint32_t, 0x123),
 *                              FRAME_DATA_SET(int64_t, -1234),
 *                              FRAME_DATA_SET(uint64_t, 0x12345678),
 *                              FRAME_DATA_SET(float_t,  0.123),
 *                              FRAME_DATA_SET(double_t, 0.123),
 *                              FRAME_DATA_SET(bytes_t,  bytes), sizeof(bytes),
 *                              FRAME_DATA_SET(string_t, "123456"));              
 *
 * @return 返回 : 填充数据的个数
 */
int frame_set_smart(frame_t *f, size_t data_cnt, ...)
{
	frame_cmd_t cmd_para;
    va_list v1;
    uint8_t *ptr = NULL;
    uint8_t *ptr_max = NULL;
    int ret = 0;
    data_item_t data_type;
	
	ASSERT(f);
	
    va_start(v1, data_cnt);

	cmd_para = (frame_cmd_t)va_arg(v1, int);
	if((--data_cnt) == 0){
		return -1;
	}
	
	
//    ASSERT(((uint32_t)cmd_para >= (uint32_t)FRAME_CMD(BEGIN)) && 
//		((uint32_t)cmd_para <= (uint32_t)FRAME_CMD(END)));
	
	frame_log("%d %d %d", cmd_para, FRAME_CMD(BEGIN), FRAME_CMD(END));
	
	
#if (FRAME_USING_DATA_TYPE_CRC == 1)
    uint8_t data_type_list[16] = {0};
    size_t data_type_cnt = 0;
#endif

    /* 存放数据的位置 */
    ptr = f->data;
    ptr_max = ptr + FRAME_DATA_FEILD_MAX_LEN;
    /* 填充cmd */
    FRAME_CMD_PARA_SET(f, cmd_para);
    FRAME_SOF_SET(f, FRAME_SOF);

#define _FRAME_DATA_SET(DATA_TYPE, SET_FUN, ARG_TYPE, ARG, PTR)                \
                                do{                                           \
                                    DATA_TYPE data;                           \
                                    data = (DATA_TYPE)va_arg(ARG, ARG_TYPE);  \
                                    SET_FUN(PTR, data);                       \
                                    PTR += sizeof(DATA_TYPE);                 \
                                }while(0)

    /* 循环解析各参数 */
    while(data_cnt){
        /* 获取数据类型 */
        data_type = (data_item_t)va_arg(v1, uint32_t);
		if((--data_cnt) == 0){
			break;
		}
#if (FRAME_USING_DATA_TYPE_CRC == 1)
        data_type_list[data_type_cnt++] = (uint8_t)data_type;
#endif
        /* 根据不同的数据类型再提取出不同的数据，写入到frame中去 */
		/* 如果少于2个，则非法输入的参数 */
		if((data_cnt--) == 0){
			ret = -1;
			break;
		}
        switch (data_type){
            case (DATA_ITEM(int8_t)):
            case (DATA_ITEM(uint8_t)):
            case (DATA_ITEM(char)):
                _FRAME_DATA_SET(uint8_t, set_uint8, uint32_t, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(int16_t)):
            case (DATA_ITEM(uint16_t)):
                _FRAME_DATA_SET(uint16_t, set_uint16, uint32_t, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(int32_t)):
            case (DATA_ITEM(uint32_t)):
                _FRAME_DATA_SET(uint32_t, set_uint32, uint32_t, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(int64_t)):
            case (DATA_ITEM(uint64_t)):
                _FRAME_DATA_SET(uint64_t, set_uint64, uint64_t, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(float)):
                _FRAME_DATA_SET(float, set_float, double, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(double)):
                _FRAME_DATA_SET(double, set_double, double, v1, ptr);
                ret++;
                break;
            case (DATA_ITEM(bytes_t)):
                do{
                    size_t cnt;
                    uint8_t *src_ptr;
					if((data_cnt--) == 0){
						ret = -1;
						break;
					}
                    src_ptr = (uint8_t *)va_arg(v1, int);
                    cnt = (size_t)va_arg(v1, int);

                    /* 限制踩内存 */
                    if((ptr - f->data + cnt) > FRAME_DATA_FEILD_MAX_LEN){
                        cnt = FRAME_DATA_FEILD_MAX_LEN - (ptr - f->data);
                    }
                    memcpy(ptr, src_ptr, cnt);
                    ptr += cnt;
                }while(0);
                ret++;
                break;
            case (DATA_ITEM(string_t)):
                do{
                    uint8_t *src_ptr;
                    size_t str_len;
                    src_ptr = (uint8_t *)va_arg(v1, int);
                    str_len = strlen((const char *)src_ptr) + 1;
                    str_len = (str_len <= (ptr_max - ptr))? str_len: (ptr_max - ptr);
                    strncpy((char *)ptr, (char *)src_ptr, str_len);
                    /* 最后的'\0' 记得加*/
                    ptr += str_len;
                }while(0);
                ret++;
                break;
            default:
                // ESP_LOGE(TAG, "frame data type error: %u", (uint32_t)data_type);
                break;
        }
    }
    va_end(v1);

    FRAME_LEN_SET(f, (uint16_t)(ptr - f->data));

    if(frame_get_len(f) > FRAME_DATA_FEILD_MAX_LEN){
        while(1);
    }

#if (FRAME_USING_DATA_TYPE_CRC == 1)
    /* 计算数据排列crc校验 */
    FRAME_DATA_TYPE_CRC_SET(f, crc_number((uint8_t *)data_type_list, data_type_cnt));
#endif

    /* 更新校验值 */
    frame_crc_update(f);
#undef _FRAME_DATA_SET

    FRAME_APPEND_EOF(f);
    return ret;
}

/**
 * 这个函数用来提取数据域中各数据
 *  
 * 
 * @param data_cnt:  待填充数据的个数
 * #param ...     :  填充的数据类型和数据
 *          exp:   frame_uint8_t, &ptr_u8,
 *                 frame_float_t, &ptr_float,
 *                 frame_bytes_t, &bytes, sizeof(bytes),
 *                 frame_string_t, &string                  
 * 推荐使用宏防止数据编译优化:  
 *         exp:    FRAME_GET_SMART(&frame,                  // 待填充的帧指针 
 *                              FRAME_CMD(CMD_NAME),   // 命令号 
 *                              FRAME_DATA_READ(int8_t,   var),
 *                              FRAME_DATA_READ(uint8_t,  var),
 *                              FRAME_DATA_READ(char_t,   var),
 *                              FRAME_DATA_READ(int16_t,  var),
 *                              FRAME_DATA_READ(uint16_t, var),
 *                              FRAME_DATA_READ(int32_t,  var),
 *                              FRAME_DATA_READ(uint32_t, var),
 *                              FRAME_DATA_READ(int64_t,  var),
 *                              FRAME_DATA_READ(uint64_t, var),
 *                              FRAME_DATA_READ(float_t,  var),
 *                              FRAME_DATA_READ(double_t, var),
 *                              FRAME_DATA_READ(bytes_t,  var), sizeof(var),
 *                              FRAME_DATA_READ(string_t, var));
 *
 * @return 返回 : 无
 */
int frame_read_smart(frame_t *f, size_t data_cnt, ...)
{
    ASSERT(f);
	
	frame_cmd_t cmd_para;

    va_list v1;
    int ret = 0;
    uint8_t *ptr = NULL;
    void *save_ptr = NULL;
    uint8_t *ptr_max = NULL;
    data_item_t data_type;
	
    va_start(v1, data_cnt);
	
	cmd_para = (frame_cmd_t)va_arg(v1, int);
	if((--data_cnt) == 0){
		return -1;
	}
	
//	ASSERT(((uint32_t)cmd_para >= (uint32_t)FRAME_CMD(BEGIN)) && 
//		((uint32_t)cmd_para <= (uint32_t)FRAME_CMD(END)));
	
#if (FRAME_USING_DATA_TYPE_CRC == 1)
    uint8_t data_type_list[16] = {0};
    size_t data_type_cnt = 0;
#endif

	if(cmd_para != frame_get_cmd_para(f)){
		frame_log("cmd para not match");
		return -1;
	}
	
    /* 存放数据的位置 */
    ptr = f->data;
    ptr_max = f->data + FRAME_DATA_FEILD_MAX_LEN;

#define _FRAME_DATA_READ(SAVE_PTR, DATA_TYPE, GET_FUN, READ_PTR)                 \
                                do{                                             \
                                    *(DATA_TYPE *)SAVE_PTR = GET_FUN(READ_PTR); \
                                    READ_PTR += sizeof(DATA_TYPE);              \
                                }while(0)

    /* 循环解析各参数 */
    while(data_cnt){
        /* 获取数据类型 */
        data_type = (data_item_t)va_arg(v1, uint32_t);
		if((--data_cnt) == 0){
			break;
		}
		
#if (FRAME_USING_DATA_TYPE_CRC == 1)
        data_type_list[data_type_cnt++] = (uint8_t)data_type;
#endif
        save_ptr  = (void *)va_arg(v1, uint32_t);
		data_cnt--;
        /* 根据不同的数据类型再提取出不同的数据，写入到frame中去 */
        switch (data_type){
            case (DATA_ITEM(int8_t)):
            case (DATA_ITEM(uint8_t)):
            case (DATA_ITEM(char)):
                _FRAME_DATA_READ(save_ptr, uint8_t, get_uint8, ptr);
                ret++;
                break;
            case (DATA_ITEM(int16_t)):
            case (DATA_ITEM(uint16_t)):
                _FRAME_DATA_READ(save_ptr, uint16_t, get_uint16, ptr);
                ret++;
                break;
            case (DATA_ITEM(int32_t)):
            case (DATA_ITEM(uint32_t)):
                _FRAME_DATA_READ(save_ptr, uint32_t, get_uint32, ptr);
                ret++;
                break;
            case (DATA_ITEM(int64_t)):
            case (DATA_ITEM(uint64_t)):
                _FRAME_DATA_READ(save_ptr, uint64_t, get_uint64, ptr);
                ret++;
                break;
            case (DATA_ITEM(float)):
                _FRAME_DATA_READ(save_ptr, float, get_float, ptr);
                ret++;
                break;
            case (DATA_ITEM(double)):
                _FRAME_DATA_READ(save_ptr, double, get_double, ptr);
                ret++;
                break;
            case (DATA_ITEM(bytes_t)):
                do{
                    size_t cnt;
					if((data_cnt--) == 0){
						ret = -1;
						break;
					}
                    cnt = (size_t)va_arg(v1, uint32_t);
                    /* 限制踩内存 */
                    if((ptr + cnt) > ptr_max){
                        cnt = ptr_max - ptr;
                    }
                    memcpy(save_ptr, ptr, cnt);
                    ptr += cnt;
                }while(0);
                ret++;
                break;
            case (DATA_ITEM(string_t)):
                do{
                    size_t str_len;
                    str_len = strlen((const char *)ptr) + 1;
                    str_len = (str_len < (ptr_max - ptr))? str_len: (ptr_max - ptr);
                    strncpy((char *)save_ptr, (char *)ptr, str_len);
                    ptr += str_len;
                }while(0);
                ret++;
                break;
            default:
                // ESP_LOGE(TAG, "frame data type error: %u", (uint32_t)data_type);
                break;
        }
    }
    va_end(v1);
#undef _FRAME_DATA_READ

#if (FRAME_USING_DATA_TYPE_CRC == 1)
    /* 计算数据排列crc校验 */
    frame_crc_t data_type_crc;
    data_type_crc =  crc_number((uint8_t *)data_type_list, data_type_cnt);
    if(data_type_crc != FRAME_DATA_CRC_GET(f)){
    }
#endif
    return ret;
}

void frame_debug_print(frame_t *f)
{
#if 1
    ASSERT(f);
    char buf[FRAME_BUFF_MAX_LEN*5] = {0};
    char tmp[4] = {0};
    uint8_t *ptr = NULL;

    ptr = (uint8_t *)f->data;
    sprintf(buf, "cmd: %x | len: %u | crc: %x data: ", frame_get_cmd_para(f),
                frame_get_len(f), FRAME_CRC_GET(f));

    for(size_t i = 0; i < frame_get_len(f); i++){
        sprintf(tmp, "%02x ", *ptr++);
        strcat(buf, tmp);
    }
	frame_log("%s", buf);
#else
#endif
}

#if (FRAME_CHECK == FRAME_CHECK_SUM_256)
static frame_crc_t crc_sum256(const uint8_t *buf, frame_len_t len)
{
	ASSERT(buf);
    frame_crc_t crc = 0;
    for(size_t i = 0; i < len; i++){
        crc += *(buf + i);
    }
    return (crc & 0xff);
}
inline static frame_crc_t crc_number(const uint8_t *buf, frame_len_t len)
{
	ASSERT(buf);
    return crc_sum256(buf, len);
}
#elif (FRAME_CHECK == FRAME_CHECK_CRC_8)
//生成多项式：X8+X5+X4+1=0x31 ，CRC8TAB[1]=生成多项式
static const uint8_t s_crc8_tab[256]={ 
    //0
    0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97, 
    0xB9, 0x88, 0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E, 
    //1
    0x43, 0x72, 0x21, 0x10, 0x87, 0xB6, 0xE5, 0xD4,
    0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 0x5C, 0x6D,
    //2
    0x86, 0xB7, 0xE4, 0xD5, 0x42, 0x73, 0x20, 0x11,
    0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8,
    //3
    0xC5, 0xF4, 0xA7, 0x96, 0x01, 0x30, 0x63, 0x52, 
    0x7C, 0x4D, 0x1E, 0x2F, 0xB8, 0x89, 0xDA, 0xEB,
    //4 
    0x3D, 0x0C, 0x5F, 0x6E, 0xF9, 0xC8, 0x9B, 0xAA,
    0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13,
    //5
    0x7E, 0x4F, 0x1C, 0x2D, 0xBA, 0x8B, 0xD8, 0xE9,
    0xC7, 0xF6, 0xA5, 0x94, 0x03, 0x32, 0x61, 0x50,
    //6
    0xBB, 0x8A, 0xD9, 0xE8, 0x7F, 0x4E, 0x1D, 0x2C,
    0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 0xA4, 0x95,
    //7
    0xF8, 0xC9, 0x9A, 0xAB, 0x3C, 0x0D, 0x5E, 0x6F,
    0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6,
    //8
    0x7A, 0x4B, 0x18, 0x29, 0xBE, 0x8F, 0xDC, 0xED,
    0xC3, 0xF2, 0xA1, 0x90, 0x07, 0x36, 0x65, 0x54,
    //9
    0x39, 0x08, 0x5B, 0x6A, 0xFD, 0xCC, 0x9F, 0xAE,
    0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17,
    //A
    0xFC, 0xCD, 0x9E, 0xAF, 0x38, 0x09, 0x5A, 0x6B,
    0x45, 0x74, 0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2,
    //B
    0xBF, 0x8E, 0xDD, 0xEC, 0x7B, 0x4A, 0x19, 0x28,
    0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 0xA0, 0x91,
    //C
    0x47, 0x76, 0x25, 0x14, 0x83, 0xB2, 0xE1, 0xD0,
    0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69,
    //D
    0x04, 0x35, 0x66, 0x57, 0xC0, 0xF1, 0xA2, 0x93,
    0xBD, 0x8C, 0xDF, 0xFE, 0x79, 0x48, 0x1B, 0x2A,
    //E
    0xC1, 0xF0, 0xA3, 0x92, 0x05, 0x34, 0x67, 0x56,
    0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF,
    //F
    0x82, 0xB3, 0xE0, 0xD1, 0x46, 0x77, 0x24, 0x15,
    0x3B, 0x0A, 0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC
}; 

/* 位循环冗余计算 */
static frame_crc_t crc_8(const uint8_t *buf, frame_len_t len)
{
	ASSERT(buf);
    uint8_t  crc = 0;
    while (len--){
        crc = s_crc8_tab[crc^*buf]; 
        buf++;
    }     

    return crc;      
}

inline static frame_crc_t crc_number(const uint8_t *buf, frame_len_t len)
{
	ASSERT(buf);
    return crc_8(buf, len);
}
#endif

#if 1
/* 将数据发送出去 */
bool frame_trans(const frame_t *f)
{
	ASSERT(f);
	
	uint8_t *ptr;
	ptr = (uint8_t *)f;
	for(size_t i = 0; i < frame_buff_len(f); i++){
		frame_byte_send(*(ptr + i));
	}
    return true;
}
#endif

bool frame_bytes_is_sof(uint8_t byte[sizeof(frame_sof_t)])
{
    return (get_uint16(byte) == FRAME_SOF);
}

bool frame_bytes_is_eof(uint8_t byte[sizeof(frame_sof_t)])
{
    return (get_uint16(byte) == FRAME_EOF);
}

frame_cmd_t frame_get_cmd_para(const frame_t *f)
{
    return (frame_cmd_t)get_uint32(f->cmd_para);
}

uint32_t frame_get_len(const frame_t *f)
{
    return (uint32_t)get_uint16(f->len);
}

uint32_t frame_buff_len(const frame_t *f)
{
    ASSERT(f);

    return frame_get_len(f) + FRAME_BUFF_MIN_LEN;
}

